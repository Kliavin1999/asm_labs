.model small 
.stack 100h
.data
    hw_string db "Hello world!", 10, 13, '$'
.code
START:
    mov ax, @data
    mov ds, ax
    mov es, ax

    lea dx, hw_string 
    mov ah, 09h
    int 21h

    mov ah, 4ch
    int 21h

end START
