#Лаборатонрые работы
#по ассемблеру

* ***Лабораторная работа №1***
 **Тема:** *Создание прострой программы на языке ассемблера* 
 **Задание** : Написать программу «Hello, world!»
 
 * ***Лабораторная работа №2***
 **Тема:** *Обработка символьных данных* 
 **Задание:** Заменить заданное слово в строке на другое заданное слово.
 **Требования:** 
	1. Выделить буфер для хранения 200 символов.
	2. Строку символов ввести с клавиатуры, при этом ввод строки символов может быть завершен клавишей Enter или по заполнению буфера полностью.
	3. Дополнительный буфер для хранения промежуточных результатов обработки строки в памяти не выделять.
	4. При использовании констант задавать их с помощью директивы EQU.
	5. Старт программы, ввод-вывод данных и обработку ошибок оформлять выводом в консоль поясняющих строк.
 
 * ***Лабораторная работа №3***
 **Тема:** *Целочисленные арифметические операции и обработка массивов числовых данных.* 
 **Задание:** Ввести массив целых чисел размерностью 30 элементов. Подсчитать число элементов, значения которых лежат в заданном диапазоне.
 **Требования:**
	1. Вид буфера для хранения массива и адресацию для доступа к его элементам выбрать самостоятельно.
	2. Числовые данные вводятся с клавиатуры в виде строк символов (по умолчанию используется десятичная система счисления), при этом требуется производить проверку на переполнение разрядной сетки числа (по умолчанию используются знаковые 16-битные данные), для знаковых данных знак требуется хранить в представлении самого числа (в дополнительном коде).
	3. При вводе числовых массивов можно указать число вводимых элементов.
	4. При операциях с целыми числами требуется проверять полученный результат на возникновение ошибок и переполнений.
	5. Формирование чисел с дробной частью по условию задачи выполнять в виде массива символов на основе только целочисленных арифметических операций (без использования FPU). Выполнить округление полученного числа до N-го символа дробной части.
	
* ***Лабораторная работа №4***
 **Тема:** *Создание видеоигры* 
 **Задание:** Написать игру 2048.
 **Требования:** 
 	1. Видеоигра должна иметь простую логику работы и только одно игро-
вое поле (уровень).
	2. Для работы с игровым полем использовать прямой доступ к видеопамяти в текстовом режиме (желательно 80×25 символов).
	3. Для отображения объектов подобрать адекватные символы, а также установить отвечающие ситуации атрибуты – цвет, моргание.
	4. Игровое поле должно также предоставлять игроку дополнительную информацию (счет, сообщения и т. п.).
	5. Желательно рассмотреть работу с системными часами или таймером (с целью формирования задержек игрового цикла, а также генерации случайных чисел).
	
* ***Лабораторная работа №5***
 **Тема:** *Работа с файлами* 
 **Задание:** Заменить во всем файле заданное слово на другое заданное слово.
 **Требования:** 
 	1. Параметры обработки (включая описание путей к файлам) передавать в программу через параметры командной строки.
	2. Буфер для обработки данных должен быть меньше обрабатываемого файла.
	3. Под словом будем понимать набор символов, ограниченный пробелами, табуляциями, границами строки; максимальный размер слова – 50 символов.
	4. Алгоритм обработки должен выполнять проверку на возникновение ошибок.
	5. Старт программы, ввод-вывод данных и обработку ошибок оформлять выводом в консоли поясняющих строк.
	6. Для тестирования программы подготовить файл размером больше 64 Кбайт.
	
* ***Лабораторная работа №6***
 **Тема:** *Интерфейс с языками высокого уровня. Работа с математическим сопроцессором.* 
 **Задание:** Ввести массив чисел с плавающей точкой на 10 элементов. Для каждого элемента массива вычислить $$Y= 1 / Xi $$
 **Требования:** 
 	1. Головная программа, включающая описание переменных, ввод-вывод данных и вызов ассемблерной процедуры, должна быть реализована на языке программирования C\C++.
	2. Реализовать основной алгоритм обработки данных на математическом сопроцессоре как процедуру на языке ассемблера. Создание тела процедуры допускается с использованием встроенного ассемблера (актуально для голов-
ной программы, написанной для Windows).
	3. Алгоритм обработки данных должен выполнять проверку на возникновение ошибок.
	4. Для головной программы, написанной для Windows (модель памяти FLAT), в процедуре использовать работу с расширенными регистрами процессора.
	5. Старт программы, ввод-вывод данных и обработку ошибок оформлять выводом в консоли поясняющих строк.
	
* ***Лабораторная работа №7***
 **Тема:** *Загрузка и выполнение программ. Работа с памятью.* 
 **Задание:** Написать программу, запускающую саму себя N раз (N – число в диапазоне [1...255]). При запуске и окончании программы выдавать номер текущей копии.
 **Требования:** 
 	1. Для передачи изменяемых параметров в программу использовать командную строку.
	2. Алгоритм обработки данных должен выполнять проверку на возникновение ошибок.
	3. Старт программы, ввод-вывод данных и обработку ошибок оформлять выводом в консоли поясняющих строк.
	
* ***Лабораторная работа №8***
 **Тема:** *Обработчики прерываний и резидентные программы.* 
 **Задание:** Написать резидентную программу проверки правильности расстановки скобок в строках консоли. Ошибки выделять цветом.
 **Требования:** 
	1. Для передачи параметров в программу использовать командную строку.
	2. Алгоритм обработки данных должен выполнять проверку на возникновение ошибок.
	3. Резидентная программа должна строить обработчик перехваченного прерывания с учетом других возможных резидентных программ, связанных с этим прерыванием.
	4. Вывод данных на экран в резидентной части программы рекомендуется не выполнять с использованием прерываний DOS, лучше использовать прямой доступ к видеопамяти.
	5. Старт программы, ввод-вывод данных и обработку ошибок оформлять выводом в консоли поясняющих строк.